import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';

import { CounterComponent } from './counter/counter.component';
import { createCustomElement } from '@angular/elements';

@NgModule({
  declarations: [
    CounterComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [
    CounterComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector) {
    const counterComponent = createCustomElement(CounterComponent, {injector});
    customElements.define('space-counter', counterComponent);
  }

  ngDoBootstrap() {}
}
