import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { timer } from 'rxjs';
import { map, startWith, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'space-counter',
  template: `
    <div [ngSwitch]="state">
      <div *ngSwitchCase="'started'" class="counting">Counting... {{currentSeconds}}</div>
      <div *ngSwitchCase="'finished'">Finished! <button (click)="start()">Restart counting</button></div>
      <div *ngSwitchDefault>Ready... <button (click)="start()">Start counting</button></div>
    </div>
  `,
  styles: [`
    .counting {color: red;}
  `]
})
export class CounterComponent {
  @Input() seconds: string|number;
  @Output() started = new EventEmitter<number>();
  @Output() finished = new EventEmitter<number>();
  state: 'idle' | 'started' | 'finished' = 'idle';
  currentSeconds: number;

  constructor(private cd: ChangeDetectorRef) { }

  start() {
    this.state = 'started';
    this.started.emit(Date.now());
    timer(0, 1000).pipe(
      startWith(-1),
      map((epleased) => +this.seconds - epleased),
      takeWhile((remaining) => remaining > 0)
    ).subscribe({
      next: (seconds) => this.update(seconds),
      complete: () => this.complete()
    });
  }

  private update(seconds) {
    this.currentSeconds = seconds;
    this.cd.detectChanges();
  }

  private complete() {
    this.state = 'finished';
    this.finished.emit(Date.now());
    this.cd.detectChanges();
  }
}
